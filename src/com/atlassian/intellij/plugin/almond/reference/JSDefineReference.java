package com.atlassian.intellij.plugin.almond.reference;

import com.atlassian.intellij.plugin.almond.model.JSIndexSearchResult;
import com.atlassian.intellij.plugin.almond.utils.ElementPatterns;
import com.atlassian.intellij.plugin.almond.utils.JSIndexSearcher;
import com.intellij.lang.javascript.psi.JSLiteralExpression;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.text.StringUtil;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementResolveResult;
import com.intellij.psi.PsiReferenceBase;
import com.intellij.psi.ResolveResult;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Set;

public class JSDefineReference extends PsiReferenceBase.Poly<JSLiteralExpression>
{
    private final String keyword;

    public JSDefineReference(PsiElement psiElement) {
        super((JSLiteralExpression) psiElement);
        keyword = StringUtil.unquoteString(psiElement.getText());
    }

    @NotNull
    @Override
    public ResolveResult[] multiResolve(boolean b) {
        JSLiteralExpression element = getElement();
        Project project = element.getProject();

        Set<JSIndexSearchResult> results = JSIndexSearcher.getRequires(keyword, project);
        List<PsiElementResolveResult> requireReferences = ElementPatterns.getRequireReferences(results, project);
        return requireReferences.toArray(new ResolveResult[requireReferences.size()]);
    }

    @Override
    public boolean isReferenceTo(PsiElement element) {
        return element instanceof JSLiteralExpression && keyword.equals(StringUtil.unquoteString(element.getText()));
    }

    @NotNull
    @Override
    public Object[] getVariants() {
        return new Object[0];
    }

    @Override
    public boolean isSoft() {
        return true;
    }
}
