package com.atlassian.intellij.plugin.almond.indexer;

import com.intellij.util.indexing.DataIndexer;
import com.intellij.util.indexing.FileContent;
import gnu.trove.THashMap;
import org.jetbrains.annotations.NotNull;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class JSRequireDataIndexer implements DataIndexer<String, Void, FileContent>
{
    private static final Pattern DEFINE_ARGS = Pattern.compile("define[ \\r\\n]*\\([ \\r\\n]*['\"].*['\"][ \\r\\n]*,[ \\r\\n]*\\[([\\s\\S]*?)\\]");
    private static final Pattern ARGUMENT_STRING = Pattern.compile("['\"](.*?)['\"]");
    private static final Pattern REQUIRE_REGEX = Pattern.compile("require[ \\r\\n]*\\([ \\r\\n]*['\"](.*?)['\"]");

    @NotNull
    @Override
    public Map<String, Void> map(@NotNull FileContent fileContent) {
        final Map<String, Void> cache = new THashMap<String, Void>();
        CharSequence textContent = fileContent.getContentAsText();

        // collect define dependencies
        Matcher argsMatcher = DEFINE_ARGS.matcher(textContent);
        while (argsMatcher.find())
        {
            String argString = argsMatcher.group(1).trim();
            Matcher argStrMatcher = ARGUMENT_STRING.matcher(argString);
            while (argStrMatcher.find())
            {
                String key = argStrMatcher.group(1).trim();
                cache.put(key, null);
            }
        }

        // collect require references
        Matcher matcher = REQUIRE_REGEX.matcher(textContent);
        while (matcher.find())
        {
            String key = matcher.group(1).trim();
            cache.put(key, null);
        }

        return cache;
    }
}
